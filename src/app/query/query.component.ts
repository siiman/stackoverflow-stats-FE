import { Component, OnInit } from '@angular/core';

import { StatisticsService } from '../statistics.service';
import { TagsService } from '../tags.service';

@Component({
  selector: 'app-query',
  templateUrl: './query.component.html',
  styleUrls: ['./query.component.css']
})
export class QueryComponent implements OnInit {
  answersByDayNameOfToday: any[]; //rename
  answersByDayNameOfTomorrow: any[]; //rename
  count: any[];
  date = Date();
  errorMsg: string;
  errorType: string;
  fetching: boolean = false;
  mostAnswered: any[];
  mostViewed: any[];
  today = this.findDayNameOfToday();
  tomorrow = this.findDayNameOfTomorrow();
  validTags: string[];

  constructor(private statsService: StatisticsService, private tagsService: TagsService) {
  }

  fetch(tag: string) {
    this.fetching = true;
    this.statsService.getStats(tag).subscribe(
      data => {
        this.count = data.count;
        this.mostViewed = data.most_viewed;
        this.mostAnswered = data.most_answered;
        this.answersByDayNameOfToday = data.answered_or_not.find(e => e.name == this.today);
        this.answersByDayNameOfTomorrow = data.answered_or_not.find(e => e.name == this.tomorrow);
      },
      e => this.setError(e, 'danger'),
      () => {
        this.fetching = false;
        this.checkForData();
      })
  }

  checkForData(): void {
    if (!this.isArrayWithData()) {
      this.setError('Sorry, no data!', 'warning');
    }
  }

  findDayNameOfToday(): string {
    let date = new Date();
    return date.toLocaleDateString(undefined, { weekday: 'long' });
  }

  findDayNameOfTomorrow(): string {
    let date = new Date();
    date.setDate(date.getDate() + 1)
    return date.toLocaleDateString(undefined, { weekday: 'long' });;
  }

  getTags() {
    this.tagsService.getTags().subscribe(
      tags => this.validTags = tags,
      e => this.setError(e, 'danger')
    )
  }

  isArrayWithData(): boolean {
    return this.count && this.count.length > 0;
  }

  nullPreviousErrorMessages() {
    this.errorMsg = '';
  }

  ngOnInit() {
    this.getTags();
  }

  setError(msg: string, type: string) {
    this.errorMsg = msg;
    this.errorType = type;
  }
}
