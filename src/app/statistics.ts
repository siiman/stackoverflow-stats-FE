export interface Statistics {
    answered_or_not: any[];
    count: any[];
    most_answered: any[];
    most_viewed: any[];
}