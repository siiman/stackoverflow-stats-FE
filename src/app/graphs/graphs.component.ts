import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-graphs',
  templateUrl: './graphs.component.html',
  styleUrls: ['./graphs.component.css']
})
export class GraphsComponent implements OnInit {
  //bar chart options
  barPadding = 2;
  gradient = false;
  groupPadding = 10;
  showLegend = true;
  showXAxis = true;
  showYAxis = true;
  showXAxisLabel = true;
  showYAxisLabel = true;
  roundEdges = false;
  scheme = 'nightLights';
  viewBar: any[] = [1500, 500];
  xAxisLabel = 'Post time UTC (24-hour clock)';
  yAxisLabel = 'Post count';
  
  //pie chart options
  schemePie = {domain: ['#5AA454', '#A10A28']};
  
  //Barchart data
  @Input() count: any[];
  
  //Pie chart data
  @Input() answersByDayNameOfToday: any[];
  @Input() answersByDayNameOfTomorrow: any[];
  
  //Days
  @Input() today: Date;
  @Input() tomorrow: Date;

  constructor() {
  }

  ngOnInit() {
  }
}
