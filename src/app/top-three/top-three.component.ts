import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-three',
  templateUrl: './top-three.component.html',
  styleUrls: ['./top-three.component.css']
})
export class TopThreeComponent implements OnInit {
  columnsToDisplay = ['title', 'value'];
  @Input() mostAnswered: any[];
  @Input() mostViewed: any[];
  @Input() today: Date;
  @Input() tomorrow: Date;

  constructor() { }

  ngOnInit() {
  }
}
