import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Statistics } from './statistics';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {
  private statsUrl: string = 'http://localhost:8080/stats'
  
  constructor(private http: HttpClient) { }

  getStats(tag: string): Observable<Statistics> {
    const url: string = `${this.statsUrl}?tag=${encodeURIComponent(tag)}`;
    return this.http.get<Statistics>(url).pipe(catchError(this.handleError));
  }

  private handleError(): Observable<never> {
    return throwError('Error getting data. Please try again!');
  };
}
