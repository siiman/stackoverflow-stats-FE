import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TagsService {
  private tagsUrl: string = 'http://localhost:8080/tags';
  
  constructor(private http: HttpClient) { }

  getTags(): Observable<Array<string>> {
    return this.http.get<Array<string>>(this.tagsUrl).pipe(catchError(this.handleError));
  }

  private handleError(): Observable<never> {
    return throwError('Error getting tags. Please refresh the page!');
  };
}
