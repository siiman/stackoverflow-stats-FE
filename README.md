# StackStatistics

This single page Angular application makes a request to the API of https://github.com/siiman/stackoverflow-stats-BE and visualizes the results. 

Graphs from https://github.com/swimlane/ngx-charts

UI style from https://material.angular.io/ and https://ng-bootstrap.github.io/#/home

Demo video on Youtube: https://youtu.be/MkVujuZcKqg

Clone this and run `ng serve --open` in the project folder. By default the back end URL is localhost:8080. 
